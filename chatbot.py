import requests

def storeTraining(newPregunta, Nuv):
    key = "a99f6850-71b0-11e9-a99f-e71f9fea02325e39a2ea-230e-441f-84de-620594751d42"
    url = "https://machinelearningforkids.co.uk/api/scratch/"+ key + "/train"

    response = requests.post(url, json={ "data" : newPregunta, "label" : Nuv })

    if response.ok == False:
        # if something went wrong, display the error
        print (response.json())
        
def classify(pregunta):
    key = "a99f6850-71b0-11e9-a99f-e71f9fea02325e39a2ea-230e-441f-84de-620594751d42"
    url = "https://machinelearningforkids.co.uk/api/scratch/"+ key + "/classify"

    response = requests.get(url, params={ "data" : pregunta })

    if response.ok:
        responseData = response.json()
        topMatch = responseData[0]
        return topMatch
    else:
        response.raise_for_status()

newPregunta = input("pregunta algo: ")
resp = classify(newPregunta)

confi = resp["confidence"]

if(resp ["class_name"] == "Campeones" and confi > 50):
    print("si")

elif(resp ["class_name"] == "jugabilidad" and confi > 50):
    print("-Después de unirte a una partida debes elegir un campeón.Elígelo basándote en cómo juegas y en lo que tu equipo necesita." +"\n -Hay muchos roles. Cuando ingreses por primera vez al mapa, serás colocado a la par de la tienda, en donde debes comprar algunos artículos." +"\n -Te mueves con el anticlick y con q,w,e,r podrás activar las habilidades del campeón ")

elif(resp ["class_name"] == "tipo_de_juego" and confi > 50):
    print("-League of Legends es un juego competitivo en línea de ritmo frenético, que fusiona la velocidad y la intensidad de la estrategia en tiempo real (ETR) con elementos de juegos de rol."+ "\n -Dos equipos de poderosos campeones, cada uno con un diseño y estilo de juegos únicos, compiten cara a cara a través de diversos campos de batalla y modos de juego.")

elif(resp ["class_name"] == "Duracion" and confi > 50):
    print("No hay limite de tiempo pero lo más común en el juego 5v5 son de 25-45 minutos  pero en el competitivo suele durar hasta 1 hora.En Aram 15-25 minutos es lo más común.  ")

elif(resp ["class_name"] == "Comunidad" and confi > 50):
    print("En lol hay gente que insulta a otros jugadores por no saber jugar o porque le hicieron perder solo una partida."+"\n -Fuera del juego la gente se reúna para ver partidas y jugar lol ")

elif(resp ["class_name"] == "Requerimientos" and confi > 50):
    print("Como minimo se necesita un Sistema operativo:"+"\n -Windows XP o Vista en adelante" +"\n -Procesador: 2 GHz "+ "\n -Memoria: 1GB de RAM " + "\n -Espacio en disco duro: 750 MB "+ "\n -Tarjeta Gráfica: Compatible con DirectX 9.0")

elif(resp ["class_name"] == "informaion" and confi > 50):
    print("Es un juego online/multijugador donde 5 personas se enfrentan contra otras 5, con el objetivo de destruir su “Nexo” obteniendo ventajas a través de objetivos como dragones torretas." + "\n -Tambien obtenemos oro matando a los enemigos ")

elif(resp ["class_name"] == "MejorJugador" and confi > 50):
    print("Faker Sanghyeok está considerado como el mejor jugador de League of Legends del planeta y posiblemente la figura más reconocida de los deportes electrónicos en el mundo." + "\n El tres veces campeón del mundo atraviesa desde hace meses un momento complicado en SK Telecom T1 y la presión puede haber hecho en el un jugador del que todo el mundo espera que por sí solo gane cualquier partida")

elif(resp ["class_name"] == "MejorEquipo" and confi > 50):
    print("No se puede decir quien es el mejor equipo." +"\n-Pero la gran mayoría de gente dice que es SK Telecom T1")

elif(resp ["class_name"] == "amigos" and confi > 50):
    print("En lol puedes hacer un monton de amigos y los pudes agregar a tu lista de invocadores." "\n Hay una forma de saber si están conectados es con un punto verde al lado")

elif(resp ["class_name"] == "tiendaDentro" and confi > 50):
    print("En la tienda de lol apareceran las cosas puedes comprar para tu campeon, para que el mismo haga más daño, o tenga mas defensa, velocidad, etc."+"\n -Se pueden comprar hasta 5 items como maximo")

elif(resp ["class_name"] == "tiendaFuera" and confi > 50):
    print("Podes comprar Riot Points, que sería la moneda virtual de juego" +"\n Estos se consiguen poniendo plata  y con estos podes comprar muchas cosas, como campeones, skin, etc." "\n También están la esencia azul que también es un forma de paga en el juego sin poner plata de tu bolsillo que nada mas podras comprar campeones")

else:
    print("En este momento no estoy capasitado con esa informcion")
    Nuv = input("En cuales de las categorias iria tu respuesta: ")
    storeTraining(newPregunta,Nuv)
    
##flagg = True
##dic = {"Jugabilidad":"RESPUESTA", "Campeones":"RESPUESTA", "tipo_de_juego":"RESPUESTA", "Duracion":"RESPUESTA","informaio":"RESPUESTA","Comunidad":"ReSpUeStA","Requerimientos":"RESpuesta"}
##while(flagg == True):
    ##flag = False
    ##preg= input("pregunta algo: ")
    ##res = classify(preg)
    ##if(res["class_name"]in dic):
        ##flag = True
        ##if(res["confidence" > 50]):
            ##categoria_eleg = res["class_name"]
            ##print(dic[categoria_eleg])
            ##flagg = True
        ##else:
            ###print("no lo se")
            ##nueva_categoria = input("En que categoria deberia ir? ")

            #hacer que ofresca categorias para podes aprender por si sola